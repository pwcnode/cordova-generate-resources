# cordova-resource-generators
<!--Cordova generators for app icons and splash screens (can be used as hooks, or not)-->
Quickly generate mobile app icons and splash screens for Cordova builds and have them automatically copied to the appropriate `/platforms/*` directories.

* You must have [ImageMagick](http://www.imagemagick.org/) installed
* Doesn't use or provide hooks.
* Creates a `/resources/` directory as a sibling to `/www/` rather than beneath it
* Also copies icons and splash screens from `/resources/` to the appropriate `/platforms/*` directories (or will create the directory, if the commands are not run from the root of your app)
* Currently only generates iOS and Android resources
* Currently only tested on OS X
* Made for Cordova 3.4 and newer

## Version
1.0

## Installation on OS X
```sh
$ curl https://bitbucket.org/pwcnode/cordova-generate-resources/raw/27b2d8bd6684e1e334367c5a3aef97cfdbd8d206/install.sh | sh
```
_By all means, examine the [install.sh](install.sh) file before installing._

## Usage
### Generate Icons
```sh
$ cd path/to/your/app
$ cordova-generate-icons path/to/your/img.png
```

### Generate Splash Screens
```sh
$ cd path/to/your/app
$ cordova-generate-splash-screens path/to/your/img.png
```
<!--
## Easter Egg Aliases
_you know, for your .bash_profile or .bashrc or .profile_
```sh
alias cgicons="cordova-generate-icons"
alias cgscreens="cordova-generate-splash-screens"
```
-->

## Dependencies
* [ImageMagick](http://www.imagemagick.org/) - Convert, Edit, And Compose Images